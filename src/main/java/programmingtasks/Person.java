package programmingtasks;

public class Person {

    private String firstName;
    private String lastName;
    private String discriminator;


    public String getFirstName(){
        return firstName;
    }

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public String getDiscriminator(){
        return discriminator;
    }

    public void setDiscriminator(String discriminator){
        this.discriminator = discriminator;
    }


}

