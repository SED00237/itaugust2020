package programmingtasks;

import sun.security.mscapi.CPublicKey;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.Instant;
import java.util.*;

public class Program {

    private static Scanner userinput = new Scanner(System.in);

    public static void main(String[] args) {

        // Addtwonumbers();
        // Multiplythreenumbers();
        // Dividetwonumbers();
        // FindAmodulooftwonumbers();
        // DisplayITupskillingninetimes();
        // PrintITUpskillingninetimes();
        // OddAndEvenNumbers();
        // ErrorHandlingWithTryCatch();
        // SelectYourbrowserName ();
        // GenerateOnetoTwentyNumbers ();

        // LoopWithIfStatement();
        // ArrayOfFiveStudents();
        // CheckUserNationality();
        // ArrayWithNoInitialValues();
        // StudentNamesWithArrayList();
        // StudentsAgeWithArrayList();
        // MixedUserData();
        // UsingMaptoPrintStudentScore();
        // UsingMarkToPrintStudentsInRange();
        // DeleteRedElement();
        // UsingParameterAndArgument();
        // RemoveElement();

        // ReadDataFromAFile();
        // WriteDataToAFile();
         fetchDataFromDataBase();


    }


    //Write a computer program to add two numbers and display the results to the consoles.
    // Variables, datatype(int), operator(+)

    public static void Addtwonumbers() {

        System.out.println("enter first number");

        int firstnumber = userinput.nextInt();
        System.out.println("first number is: " + firstnumber);

        System.out.println("Enter second number");
        int secondnumber = userinput.nextInt();
        System.out.println("second number is :" + secondnumber);

        int answer = firstnumber + secondnumber;
        System.out.println("answer is : " + answer);

    }

    ///  Task2
    /// Write a computer program to multiply any three number and display the reults to console.
    /// Variables, datatypes(long,int), operator(*).


    public static void Multiplythreenumbers() {

        System.out.println("enter first number");
        int firstnumber = userinput.nextInt();
        System.out.println("firstnumber is: " + firstnumber);

        System.out.println("enter second number");
        int secondnumber = userinput.nextInt();
        System.out.println("second number is:" + secondnumber);

        System.out.println("enter third number");
        int thirdnumber = userinput.nextInt();
        System.out.println("third number is:" + thirdnumber);

        int answer = firstnumber * secondnumber * thirdnumber;

        System.out.println("answer is :" + answer);

    }

    ///Task 3
    ///Write a computer program to divide two numbers and display to the console
    ///Variables datatypes(int), operator(/)

    public static void Dividetwonumbers() {

        System.out.println("enter first number");
        int firstmunber = userinput.nextInt();
        System.out.println("first number is: " + firstmunber);

        System.out.println("enter secondnumber");
        int secondnumber = userinput.nextInt();
        System.out.println("second is:" + secondnumber);

        int answer = firstmunber / secondnumber;

        System.out.println("answer is:" + answer);


    }

    ///Task 4
    ///Write a computer program to find a modulo of two numbers and display the reslut to the console
    ///Variables, datatypes(float, long), operator(%)

    public static void FindAmodulooftwonumbers() {

        System.out.println("A computer program to divide two numbers");

        System.out.println("Input the first number and press Enter");
        long firstnumber = userinput.nextLong();

        System.out.println("Imput the second number and press Enter");
        long secondnumber = userinput.nextLong();

        float answer = firstnumber % secondnumber;
        System.out.println("Modulo of two numbers:" + answer);

    }

    ///Task 5
    ///Write a computer program to display to the consoles "ITUpskilling" nine times
    ///Put all your codes in one method

    public static void DisplayITupskillingninetimes() {

        //    System.out.println("ITUpskilling")
        //    System.out.println("ITUpskilling");
        //    System.out.println("ITUpskilling");
        //    System.out.println("ITUpskilling");
        //    System.out.println("ITUpskilling");
        //    System.out.println("ITUpskilling");
        //    System.out.println("ITUpskilling");
        //    System.out.println("ITUpskilling");
        //    System.out.println("ITUpskilling");

    }


    ///Task 6
    ///Write a computer program to display to the console "ITUpskilling" nine times
    ///Use three methods, put each method in a new class, you are going to have three new classes


    public static void PrintITUpskillingninetimes() {

        //  FirstClass firstclass = new FirstClass(2, 3);
        //     firstclass.methodone();


        SecondClass secondclass = new SecondClass("Edward", "DEFFO");
        secondclass.secondmethod();


        //   ThirdClass thirdclass = new ThirdClass();
        //   thirdclass.thirdmethod();

    }

    ///Task7
    ///If-statement
    ///Write a computer program to ask a user to input an integer number
    ///Display to the console 'Even' or 'Odd' number base on the user input


    ///true and true = true
    ///true and false = false
    ///true or false = true
    ///false or true= true
    ///false and false = false
    ///false or false = false

    public static void OddAndEvenNumbers() {

        System.out.println("Input an integer number and press enter");

        int number = userinput.nextInt();

        if (number / 2 == 0) {

            System.out.println("this is en even number");
        } else if (number / 2 == 1) {
            System.out.println("this is an odd number");

        }
    }


    ///Task 8
    /// Try-cash statement
    ///Write a computer program to ask a user to input an integer number
    ///And display to the console 'Even' or 'Odd' number based on user input

    public static void ErrorHandlingWithTryCatch() {

        try {

            System.out.println("Input an integer number and press enter");

            int number = userinput.nextInt();

            if (number / 2 == 0) {

                System.out.println("this is en even number");
            } else if (number / 2 == 1) {
                System.out.println("this is an odd number");
            }

        } catch (Exception ex) {

            System.out.println("please enter a number only");
        }

    }


    ///Task8
    /// Switch-case statement
    /// write a computer program which allow a user to input a browser name (i.e IE,chrome,safari )
    /// Then inputted browser name can be used to run test automation
    /// Break down test task

    //(1.) Give a user an instruction about what to input
    //(2.) Collect user's input and store it to a variable string datatype
    //(3.) Used the store data to match one of the available condition
    //(4.) Execute the code within the match condition

    public static void SelectYourbrowserName() {

        System.out.println("Input your browser let it be (IE, safari, chrome or safari )");

        String browserName = userinput.nextLine().toLowerCase();

        switch (browserName) {

            case "ie":
                System.out.println("this is internet explorer");
                break;

            case "chrome":
                System.out.println("this is chrome browser");
                break;

            case "firefox":
                System.out.println("this is firefox browser");
                break;

            case "safari":
                System.out.println("this is safari browser");
                break;

            default:
                System.out.println("please enter correct browser: ");
                break;
        }

    }


    ///Task 10
    ///Task 9
    ///Debugging helps us to see what is going within our codes at run times
    ///You need to decide as a programmer or a test analyst


    ///Task11
    ///For-loop statement
    ///For  statement
    /// Write a computer program to generate 1 to 20 to the console


    public static void GenerateOnetoTwentyNumbers() {

        for (int i = 1; i <= 20; i++) {
            System.out.println(i);
        }


    }


    ///For-statement and if-statement
    ///Write a computer program to generate 1 to 20 to the console
    ///Only display all even numbers

    public static void LoopWithIfStatement() {

        for (int i = 0; i <= 20; ++i) {
            if (i % 2 == 1) {
                System.out.println("This is an Odd number: " + i);
            }
        }
    }
    ///Task 13
    ///Collections (Array, list, Arraylist, Hashmap)
    ///An array can be used to group data of a known number i.g: 5 students records
    ///We can have array of different datatypes i.e( int, float, sting, boolean)
    ///string [] and int [] means array of string and array of integer respectivelly


    ///Capture five student names and display them to the consoles
    ///For-each loop or for Each statement - use to display values in an array or arraylist


    public static void ArrayOfFiveStudents() {

        String[] firstnames = new String[]{"Adamma", "Edward", "Joe", "Peter", "Azeez"};
        ///  System.out.println(firtsnames);

        for (String names : firstnames) {
            System.out.println(names);
        }
    }


    //Assignment 1
    //Write a computer program to ask a user their nationality and write back
    //followings on the console depends on user's response
    //(1) if a user inputs 'UK' write back you are from 'EU'
    //(2) if a user inputs 'Cameroon' write back you are from 'Africa'
    //(3) if a user inputs 'Mexico' write back you are from the 'Rest of the world'
    //(4) if a user inputs didn't give correct input, then write back your input is incorrect


    public static void CheckUserNationality() {

        System.out.println("What is your nationality e.g UK, Cameroon, Mexico");

        String CountryOfOrigin = userinput.nextLine();

        if (CountryOfOrigin.equalsIgnoreCase("UK")) {
            System.out.println("You are from EU");

        } else if (CountryOfOrigin.equalsIgnoreCase("Cameroon")) {
            System.out.println("you are from Africa");

        } else if (CountryOfOrigin.equalsIgnoreCase("Mexico")) {
            System.out.println("you are from the rest of the world");

        } else {
            System.out.println("your input is incorrect");
        }

    }

    ///Task 14
    ///Capture five student age and display them to the console
    public static void FiveStudentsAge() {

        int[] ages = new int[]{10, 45, 34, 31, 21};
        for (int age : ages) {
            System.out.println("Student age :" + age);
        }
    }

    ///Task 15
    ///Capture five students age and display them to the console
    ///only display the age that is above thirty on the console
    public static void StudentAgeAboveThirty() {

        int[] ages = new int[]{10, 45, 43, 31, 21};
        for (int age : ages) {
            if (age > 30) {
                System.out.println("Student Age above 30 : " + age);
            }

        }
    }

    ///Task 16
    ///Declare arrays of three strings with no initial values
    ///Print those strings to the console using for string statement
    ///Then assign to two strings out of three
    ///Print them to the

    ///Finally assign a value to the last variable of the string
    ///And print all values to the console
    ///Array uses index to stores data i.g 0-2 means 3


    public static void ArrayWithNoInitialValues() {

        String[] firstNames = new String[3];

        firstNames[0] = "Chantal";
        firstNames[1] = "Nadia";

        for (String firstName : firstNames) {
            System.out.println("print 2 values : " + firstName);
        }

        //A new line
        System.out.println();

        firstNames[2] = "Edward";
        for (String firstName : firstNames) {
            System.out.println("print with third value : " + firstName);
        }

    }

    ///Task 17
    ///List (as collection) it can use to manage group of data with similar data type
    ///The data type can be string, integer, boolean, float, etc. this not a fixed length.
    ///Collection (unlike an array). List can evolve grow i.e you can add and remove data from a list
    ///Write a computer program to capture three students name and display them to the console.
    ///Add three more students to the list and display them all to the console
    ///Remove three students from the list


    public static void StudentNamesWithArrayList() {

        List<String> studentNames = new ArrayList<>(Arrays.asList("Adrien", "Briand", "Arnaud", "Mallo", "Lea"));

        for (String student : studentNames) {
            System.out.println("Print out student name : " + student);
        }

        System.out.println();

        studentNames.add("Aroon");
        studentNames.add("Marissa");
        studentNames.add("Michel");
        for (String student : studentNames) {
            System.out.println("print all name : " + student);
        }

        System.out.println();

        studentNames.remove("Mallo");
        studentNames.remove(2);

        for (String student : studentNames) {
            System.out.println("print out remaining names : " + student);
        }
    }

    ///Write a computer program to capture three students age and display to the console
    ///Add more three students age to the list and display them to the console
    ///Remove some age from the list

    public static void StudentsAgeWithArrayList() {

        List<Integer> studentAge = new ArrayList<>(Arrays.asList(24, 45, 21));
        for (Object Age : studentAge) {
            System.out.println("Print student age : " + Age);
        }

        System.out.println();

        studentAge.add(56);
        studentAge.add(34);
        studentAge.add(25);

        for (Object Age : studentAge) {
            System.out.println("Print all ages : " + Age);
        }

        System.out.println();

        studentAge.remove(5);
        studentAge.remove(0);

        for (Object Age : studentAge) {
            System.out.println("Print remaining Ages : " + Age);
        }

    }

    ///Task 19
    ///Write a computer program to print to the console a set of data with a different datatypes
    ///Print out just string data for the six list

    public static void MixedUserData() {

        ArrayList mixdata = new ArrayList(Arrays.asList("Lesli", 23, "Nadia", 56, true, false, "Adrien"));
        for (Object mixUserdata : mixdata) {
            System.out.println("mixed of student data :" + mixUserdata);
        }

        System.out.println();

        for (Object mixUesrdata : mixdata) {
            if (mixUesrdata instanceof String) {
                System.out.println("print out all string data : " + mixUesrdata);
            }

            if (mixUesrdata instanceof Integer) {
                System.out.println("print out all integers : " + mixUesrdata);
            }

            if (mixUesrdata instanceof Boolean) {
                System.out.println("print out all Boolean : " + mixUesrdata);
            }
        }

    }
    ///Task 20
    ///Map is a collection it can be used to manage two data (e.g key and value)
    ///for a single variable in a list. Two data will be required when you have a map
    ///for studentName and studentScore will be attract both String and Integer respectivelly
    ///Write a program to capture name with their marks.

    public static void UsingMaptoPrintStudentScore() {

    }

    ///Task 21
    ///Write a program to capture student name with their marks
    ///Display something to the console like ( student Joe has a score of 70)
    ///Display student that has score which is more than 40 and below 70


    public static void UsingMarkToPrintStudentsInRange() {

        Map<String, Integer> studentScores = new HashMap<>();

        studentScores.put("Leslie", 70);
        studentScores.put("Adama", 30);
        studentScores.put("peter", 45);
        studentScores.put("Malven", 90);
        studentScores.put("Salomom", 80);

        for (Map.Entry<String, Integer> studentsMarks : studentScores.entrySet()) {
            if (studentsMarks.getValue() > 40 && studentsMarks.getValue() < 70) {
                System.out.println("Student with their mark : " + studentsMarks.getKey() + " has a score of " + studentsMarks.getValue());
            }
        }
    }
    ///Task22
    ///While statement is an iteration statement (like for, for each) can be used to loop through
    ///a set of data so as to meet certain condition
    ///Print out congratulations to the console 20 times using for and while statements

    public static void ForLoopToPrintOutCongrat() {

        for (int i = 1; i <= 20; i++) {
            System.out.println("congratulations : " + i);
        }

        System.out.println();

        int counter = 1;
        while (counter <= 20) ;
        System.out.println("congratulation!");
        counter++;
    }


    ///Task 23
    ///Method or function (void, return, parameter, argument)
    ///A void method is the one that returns no value whenever is called or referenced
    ///A method that is capable of give back a value is called a return method
    ///Parameter are a variable(s) you declare at point of creating a method


    ///Write a computer program to add two numbers. Create a method to do this in an another class
    ///Create two variables in the chosen class to get data
    ///Display the additional result to the console


    public static void UsingParameterAndArgument() {

        FirstClass firstclass = new FirstClass();
        //firstclass.AddTwoNumberCal(2, 2);

        String name = firstclass.studentName();

        System.out.println(name);

    }


    ///Assignment2
    ///Write a Java program that will delete element (red) from a collection list of elements (make use of Array List here)
    //   a. store these elements in a list (blue, yellow, red, white, green)
    //   b. print out these elements
    //   c. delete element red from list
    //   d. print out the elements after deletion


    public static void RemoveElement() {


        List<String> mixdata = new ArrayList<>(Arrays.asList("blue", "yellow", "red", "white", "green"));

        // for (Object element : mixdata) {
        //          System.out.println(element);

        int Arraysize = mixdata.size();
        //   System.out.println(Arraysize);

        for (int i = mixdata.size() - 1; i >= 0; i--) {

            if (mixdata.get(i).equals("green")) {
                mixdata.remove(i);
            }

            if (mixdata.get(i).equals("red")) {
                mixdata.remove(i);
            }
        }
        System.out.println(mixdata);

    }


    ///Task 24


    ///Class(instance or static)
    ///Instance of a class will allow you to create an object
    ///For example FirstClass, SecondClass, are all instance of classes
    ///Static class will not allow you to create an object for it. i.e there will not be an instance of a static class.
    ///We can have static variable and static method
    ///You can only have static class as a subclass of a class.


    public static void InstanceAndStaic() {

        SecondClass secondclass = new SecondClass("edward", "deffo");
        //    secondmethod();
        SecondClass.secondmethod();

    }

    /// Task 25
    ///String manipulations:
    ///Write a program to declare a string variable and use that variable for various thing(around a string datatype)


    public static void StringManipulation() {

        String name = "Edward is manipulating strings";

        System.out.println("enter a string value");

        String value = userinput.nextLine();

        //    System.out.println(value.contains("lon")); true or false = boolean
        //    System.out.println(value.length()); Give back the length of a string
        System.out.println(value.substring(1, 3));
        System.out.println(value.substring(0, 6));

        //   This is a real life example we are using CMS to to manipulate strings.

        if (value.contains("Student_V2")) {
            System.out.println("Do something");
        } else if (value.contains("Student_V2")) {
            System.out.println("Do Something");
        } else {
            System.out.println("is Enrollment");
        }

        String newitem = value.substring(0, value.indexOf("_V2"));

    }

    /// Task 26
    /// Write a computer program to read data from a file 'ReadFromAFile' and
    ///Display its content in console


    public static void ReadDataFromAFile() {

        StringBuilder sb = new StringBuilder();

        try {

            BufferedReader br = Files.newBufferedReader(Paths.get("C:\\ITUskilling2020\\itaugust2020\\src\\main\\resources\\TestData\\testdatafile.txt"));

            {
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            }

        } catch (IOException ex) {
            System.err.format("IOException occured please check : ", ex);
        }

        System.out.println(sb);

    }

    ///Task 27

    public static void WriteDataToAFile() {

        try {
            List<String> list = Arrays.asList("Line1", "Line2", "Upskilling2020");

            Files.write(Paths.get("C:\\ITUskilling2020\\itaugust2020\\src\\main\\resources\\TestData\\TestData2"), list);
        } catch (IOException ex) {

            System.err.format("Exception occured please check : ", ex);
        }
    }

    ///Task 28
    ///How to communicate with SQL server DataBase:
    ///1.Add dependency for sql server using pom
    ///2.Add a sql server library to java
    ///3.Write code to connect database
    ///4.fetch data from database table and print it out to the console


    public static void fetchDataFromDataBase(){

          try {
              String url = "jdbc:sqlserver://LAPTOP-3RJB38JI\\SQLEXPRESS;databaseName=CourseManagementSystem.Models.SchoolContext;integratedSecurity=true";

              Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
              Connection con = DriverManager.getConnection(url);

              if (con != null) {
                  System.out.println("System connected successfully");
              }

              String queryString = "select * person";
              Statement sqlStatement = con.createStatement();
              ResultSet sqlResult = sqlStatement.executeQuery(queryString);

              while(sqlResult.next()){

                  Person person = new Person();
                  person.setFirstName(sqlResult.getString(1));
                  person.setLastName(sqlResult.getString(2));
                  person.setDiscriminator(sqlResult.getString(3));

                  System.out.println(person.getFirstName());

              }

          }catch (Exception ex){
              ex.getMessage();

          }

    }

}